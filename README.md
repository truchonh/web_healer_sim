# Mana regen planer

## Assumed information:
- "correct" talents for each specs (ex: intensity and living spirit for druids)
- Gradual and even mana usage (warning to bursty specs coh priests)
- Ideal use of Innervate, aka enaugh spirit to regen your entire mana bar and used when mana is empty
- Improved versions of each class buffs
- /!\ No mana drain in the fight ! /!\
- 100% uptime for mana tide and spriest
- Static mp5 values for some very stable proc items (memento, ...)

## UI requirements:
- Character info section
  - Class dropdown
- Stats section
  - Text inputs for int, spirit, raw mp5
- Proc items section
  - Multiselect for all known proc items (BD, IED, etc..)
- Buffs section
  - A bunch of checkboxes for class buffs and consumes
  - Checkbox for combat consumes usage (pot, runes)
- Additional checkbox for alchemist stone if pot is selected
  - Group utility checkboxes (spriest, mana tide, innervate [hide that one for shamies and pallies hehe])
- Casts section
  - Editable table for CPM / ability
  - Row per ability, input for cpm, checkbox for snowballs macroed
  - [if snowballs checked] Text input for JoW uptime (default to 90% or something like that)
  - Show average spell cost, total CPM and mp5 from snowballs
- Results section
  - Nice graph for mana spending rate, including expected min and max values when proc effects are selected (5 plot lines: average, 1 SD away and 2 SD away)
  - Table view with breakdown of combat time extension for each proc items / consumes / buff NOT selected

## Implementation:
- Nice little Vuejs app perhaps ?
- API routes
  - `GET 	/simulate?args={json encoded sim arguments}` returns the data for the graph and table
