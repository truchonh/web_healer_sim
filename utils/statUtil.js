

module.exports = {
    calculateSpiritMp5({ intellect, spirit }) {
        return (0.001 + (spirit * 0.009327 * Math.pow(intellect, 0.5))) * 5
    }
};