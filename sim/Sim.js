class Sim {
	get tickMs() {
		return 60 / this.cpm * 1000
	}
	
	constructor(cpm, effects = []) {
		this.cpm = Math.max(cpm, 1)
		this.effects = effects
	}
	
	run(runTimeMs) {
		const ticksInSim = Math.floor(runTimeMs / this.tickMs)
		const effectsState = this.effects.map(_ => ({ uptimeMs: 0, cdExpire: null, effectExpire: null, count: 0 }))
		
		for (let i = 0; i < ticksInSim; i++) {
			
			// calculate procs
			this.effects.forEach((effect, index) => {
				// check for effect expiration
				if (effect.duration && effectsState[index].effectExpire && effectsState[index].effectExpire < i) {
					effectsState[index].uptimeMs += effect.duration * 1000
					effectsState[index].effectExpire = null
				}
				
				// if the effect is on cd, continue
				if (effectsState[index].cdExpire && effectsState[index].cdExpire > i) {
					return
				}
				
				if (Math.random() <= effect.proc) {
					// has proced
					effectsState[index].count++;
					effectsState[index].cdExpire = effect.cd ? i + (effect.cd * 1000 / this.tickMs) : null
					
					if (effectsState[index].effectExpire) {
						// proced again before the effect expired
						const activeTime = (effect.duration * 1000) - ((effectsState[index].effectExpire - i) * this.tickMs)
						effectsState[index].uptimeMs += activeTime
					}
					
					if (effect.duration) {
						effectsState[index].effectExpire = i + (effect.duration * 1000 / this.tickMs)
					}
				}
			})
			
		}
		
		return effectsState
	}
}
module.exports = Sim