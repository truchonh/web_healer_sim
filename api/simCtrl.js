const Sim = require('../sim/Sim');
const _ = require('lodash');
const { standardDeviation, mean } = require('simple-statistics');
const statUtil = require('../utils/statUtil')

const procItems = {
    blueDragon: { name: 'Darkmoon Card: Blue Dragon', proc: 0.02, duration: 15, cd: 0 },
    bangle: { name: 'Bangle of Endless Blessings', proc: 0.1, duration: 15, cd: 50 },
    ied: { name: 'Insightful Earthstorm Diamond', proc: 0.05, cd: 15 },
    t4_2piece: { name: 'Tier 4 2-piece bonus', proc: 0.05, cd: 0 },
};
const SIM_ITERATIONS = 10000;

class simCtrl {
    static init(app) {
        app.get('/simulate', this.handleSim.bind(this));
    }

    static handleSim(req, res) {
        const simConfig = JSON.parse(req.query.config || '{}');

        const spiritMp5 = statUtil.calculateSpiritMp5({ ...simConfig });
        const inCombatMp5 = simConfig.mp5 + Math.floor(spiritMp5 * 0.3);
        const totalMp5 = inCombatMp5 +
            (simConfig.consumables.includes('manaPot') ? 100 : 0) +
            (simConfig.consumables.includes('rune') ? 50 : 0) +
            (simConfig.raidUtility.includes('spriest') ? 300 : 0) +
            (simConfig.raidUtility.includes('manaSpring') ? 50 : 0);
        const mp5Consumed = (simConfig.cpm * simConfig.spellCost) / 12;
        // TODO: Implement innervate and mana tide in a more intelligent way
        const totalMana = simConfig.totalMana +
            (simConfig.raidUtility.includes('manaTide') ? (simConfig.totalMana * 0.24) : 0) +
            (simConfig.raidUtility.includes('innervate') ? (simConfig.totalMana) : 0) +  // assume innervate fills your mana bar
            (simConfig.raidUtility.includes('shadowfiend') ? 8000 : 0); // approximated value

        let estimatedDurationSeconds = Math.min(totalMana / (mp5Consumed - totalMp5) * 5, 1800); // cap at 30 minutes
        if (totalMp5 > mp5Consumed) {
            estimatedDurationSeconds = 1800;
        }

        let calculatedUptime = this._runSim(
            estimatedDurationSeconds,
            simConfig.cpm,
            Object.values(procItems)
        );

        // re-run the sim for each item
        const selectedItems = _.filter(procItems, (item, key) => simConfig.procItems.includes(key));
        let totalMp5WithItems = totalMp5
        for (let selectedItem of selectedItems) {
            const itemName = selectedItem.name;
            const itemKey = _.findKey(procItems, item => item.name === itemName);

            const items = this._processCalculatedUptime(estimatedDurationSeconds, spiritMp5, calculatedUptime);

            totalMp5WithItems += items.find(item => item.item === itemKey).avg;
            estimatedDurationSeconds = Math.min(totalMana / (mp5Consumed - totalMp5WithItems) * 5, 1800);
            if (totalMp5WithItems > mp5Consumed) {
                estimatedDurationSeconds = 1800;
            }

            calculatedUptime = this._runSim(
                estimatedDurationSeconds,
                simConfig.cpm,
                Object.values(procItems)
            );
        }

        res.send({
            config: simConfig,
            estimatedDurationSeconds: Math.round(estimatedDurationSeconds),
            items: this._processCalculatedUptime(estimatedDurationSeconds, spiritMp5, calculatedUptime),
        });
    }

    static _runSim(runtimeSeconds, cpm, procItems) {
        let regenSim = new Sim(cpm, procItems)

        let statesGroup = []
        for (let i = 0; i < SIM_ITERATIONS; i++) {
            statesGroup.push(regenSim.run(runtimeSeconds * 1000))
        }

        let calculatedUptime = []
        for (let stateIndex = 0; stateIndex < statesGroup[0].length; stateIndex++) {
            let values = statesGroup.map(_states => _states[stateIndex].uptimeMs)
            calculatedUptime.push({
                mean: mean(values),
                deviation: standardDeviation(values),
                min: Math.min(...values),
                max: Math.max(...values),
                count: mean(statesGroup.map(_states => _states[stateIndex].count)),
                countDeviation: standardDeviation(statesGroup.map(_states => _states[stateIndex].count))
            })
        }
        return calculatedUptime;
    }

    static _processCalculatedUptime(estimatedDurationSeconds, spiritMp5, calculatedUptime) {
        return calculatedUptime.map((uptimeStats, i) => {
            const itemKey = Object.keys(procItems)[i]

            return {
                item: itemKey,
                ...this._calculateMp5Values({
                    itemKey,
                    spiritMp5,
                    uptime: uptimeStats.mean / (estimatedDurationSeconds * 1000),
                    deviation: uptimeStats.deviation / (estimatedDurationSeconds * 1000),
                    procPerMin: uptimeStats.count / (estimatedDurationSeconds / 60),
                    procRateSd: uptimeStats.countDeviation / (estimatedDurationSeconds / 60),
                })
            };
        })
    }

    static _calculateMp5Values({ spiritMp5, itemKey, procPerMin, procRateSd, uptime, deviation }) {
        let avg, sig1, sig2;
        if (itemKey === 'ied') {
            avg = procPerMin * 300 / 12;
            sig1 = (procPerMin + procRateSd) * 300 / 12;
            sig2 = (procPerMin + procRateSd * 2) * 300 / 12;
        } else if (itemKey === 't4_2piece') {
            avg = procPerMin * 120 / 12;
            sig1 = (procPerMin + procRateSd) * 120 / 12;
            sig2 = (procPerMin + procRateSd * 2) * 120 / 12;
        } else if (itemKey === 'blueDragon' && spiritMp5) {
            const regenRate = 0.7; // 100%, but effectively 70% because of the Meditation talent
            avg = uptime * spiritMp5 * regenRate;
            sig1 = (uptime + deviation) * spiritMp5 * regenRate;
            sig2 = (uptime + deviation * 2) * spiritMp5 * regenRate;
        } else if (itemKey === 'bangle' && spiritMp5) {
            const regenRate = 0.15;
            avg = uptime * spiritMp5 * regenRate;
            sig1 = (uptime + deviation) * spiritMp5 * regenRate;
            sig2 = (uptime + deviation * 2) * spiritMp5 * regenRate;
        }

        return {
            avg: _.round(avg, 2),
            sig1: _.round(sig1, 2),
            sig2: _.round(sig2, 2),
        }
    }
}

module.exports = simCtrl;
