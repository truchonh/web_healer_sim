const networkStatus = {
    idle: 'idle',
    loading: 'loading',
    succeeded: 'succeeded',
    error: 'error'
}
export default Object.freeze(networkStatus)
