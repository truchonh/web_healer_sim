export const items = {
    blueDragon: { name: 'Darkmoon Card: Blue Dragon', proc: 0.02, duration: 15, cd: 0 },
    bangle: { name: 'Bangle of Endless Blessings', proc: 0.1, duration: 15, cd: 50 },
    ied: { name: 'Insightful Earthstorm Diamond', proc: 0.05, cd: 15 },
    t4_2piece: { name: 'Tier 4 2-piece bonus', proc: 0.05, cd: 0, classes: ['druid'] },
}

export const buffs = {
    kings: { name: 'Blessing of Kings', modifier: 0.1 },
    motw: { name: 'Mark of the Wild', stats: { intellect: 18, spirit: 18 } },
    ai: { name: 'Arcane Intellect', stats: { intellect: 40 } },
    wisdom: { name: 'Blessing of Wisdom', stats: { mp5: 49 } },
    ds: { name: 'Divine Spirit', stats: { spirit: 50 } },
}

export const consumables = {
    draenicWisdom: { name :'Elixir of Draenic Wisdom', stats: { intellect: 30, spirit: 30 } },
    fishSticks: { name: 'Golden Fish Sticks', stats: { spirit: 20 } },
    spiritScroll: { name: 'Scroll of Spirit V', stats: { spirit: 30 } },
    direMaulBeer: { name: 'Kreeg\'s Stout Beatdown', stats: { intellect: -5, spirit: 25 } },
    manaPot: { name: 'Super Mana Potion' },
    rune: { name: 'Dark Rune' },
}

export const raidUtility = {
    spriest: { name: 'Shadow Priest' }, // 300ish mp5
    manaTide: { name: 'Mana Tide Totem' }, // 24% of total mana over 12 seconds minus mana gained from mana spring during this time
    manaSpring: { name: 'Mana Spring Totem' }, // 50 mp5
    innervate: { name: 'Innervate', classes: ['druid', 'priest'] }, // not sure exactly. Should be derived from base stats
    shadowfiend: { name: 'Shadowfiend', classes: ['priest'] }
}

export const spells = {
    druid: [
        { id: '33763', name: 'Lifebloom', cost: 176, cpm: 0, snowball: false },
        { id: '26982', name: 'Rejuvenation', cost: 332, cpm: 0, snowball: false },
        { id: '9857', name: 'Regrowth (R8)', cost: 388, cpm: 0, snowball: false },
        { id: '26980', name: 'Regrowth (R10)', cost: 540, cpm: 0, snowball: false },
        { id: '18562', name: 'Swiftmend', cost: 303, cpm: 0, snowball: false },
        { id: '33891', name: 'Tree of Life', cost: 464, cpm: 0, snowball: false },
        { id: '26979', name: 'Healing Touch', cost: 935, cpm: 0, snowball: false },
        { id: '26983', name: 'Tranquility', cost: 1650, cpm: 0, snowball: false },
        { id: '26994', name: 'Rebirth', cost: 1200, cpm: 0, snowball: false },
        { id: '26988', name: 'Moonfire', cost: 495, cpm: 0, snowball: false },
        { id: '27013', name: 'Insect Swarm', cost: 175, cpm: 0, snowball: false },
        { id: '26985', name: 'Wrath', cost: 255, cpm: 0, snowball: false },
        { id: '26986', name: 'Starfire', cost: 370, cpm: 0, snowball: false },
        { id: '783', name: 'Travel Form', cost: 215, cpm: 0, snowball: false },
    ],
    priest: [
        { id: '25235', name: 'Flash Heal', cost: 470, cpm: 0, snowball: false },
        { id: '25222', name: 'Renew', cost: 405, cpm: 0, snowball: false },
        { id: '34866', name: 'Circle of Healing', cost: 405, cpm: 0, snowball: false },
        { id: '33076', name: 'Prayer of Mending', cost: 273, cpm: 0, snowball: false },
        { id: '25218', name: 'Power Word: Shield', cost: 540, cpm: 0, snowball: false },
        { id: '2060', name: 'Greater Heal', cost: 315, cpm: 0, snowball: false },
    ]
}

export const AVG_JOW_PROC = 33

export const CLASS_BASE_MANA = {
    druid: 2370,
    priest: 2620,
}

export const classes = [
    { value: 'druid', label: 'Druid' },
    { value: 'priest', label: 'Priest' },
]
