import {TextField} from '@mui/material'
import NumberFormat from 'react-number-format'
import {forwardRef, useMemo} from 'react'
import PropTypes from 'prop-types'

export default function NumericField(props) {
    const NumberFormatCustom = useMemo(() => forwardRef((props, ref) => {
        const { onChange, ...other } = props;

        return <NumberFormat
            {...other}
            getInputRef={ref}
            onValueChange={(values) => onChange({
                target: {
                    name: props.name,
                    value: values.value ? parseInt(values.value) : null,
                },
            })}
            isNumericString
            allowNegative={false}
            decimalScale={0}
        />
    }), [])

    NumberFormatCustom.propTypes = {
        name: PropTypes.string.isRequired,
        onChange: PropTypes.func.isRequired,
    }

    return (
        <TextField
            {...props}
            InputProps={{
                inputComponent: NumberFormatCustom,
            }}
        />
    )
}