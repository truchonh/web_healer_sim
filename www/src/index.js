import React from 'react';
import { createRoot } from 'react-dom/client';
import App from './app/App';
import reportWebVitals from './reportWebVitals';
import 'fontsource-roboto';
import { Provider } from 'react-redux'
import store from './app/store'

import './lib/Chart'
import * as dayjs from 'dayjs'
import 'dayjs/locale/fr'
dayjs.locale('fr')

const root = createRoot(document.getElementById('root'))
root.render(
  <React.StrictMode>
      <Provider store={store}>
          <App />
      </Provider>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
