import _ from 'lodash'
import {AVG_JOW_PROC, buffs, CLASS_BASE_MANA, consumables, spells} from '../../models/Config'

export function selectTotalCpm(state) {
    const classSpells = spells[state.character.class]
    return _.chain(classSpells)
        .sumBy(spell => state.character.castsDistribution[spell.id]?.cpm)
        .round(1)
        .value()
}

export function selectMeanSpellCost(state) {
    const totalCpm = selectTotalCpm(state)
    return _.sumBy(spells[state.character.class], (spell) => {
        const spellConfig = state.character.castsDistribution[spell.id]
        if (spellConfig && spellConfig.cpm > 0) {
            const jowDiscount = spellConfig.snowball ? AVG_JOW_PROC : 0
            return Math.round((spell.cost - jowDiscount) * spellConfig.cpm / totalCpm)
        }
        return 0
    })
}

const getStat = (config, configKey, statKey) => {
    const configValue = config[configKey]
    if (!configValue || !configValue.stats || !configValue.stats[statKey]) {
        return 0
    }
    return configValue.stats[statKey]
}

const reduceSelectedValuesToStats = (selectedValues, config, baseStats, playerClass) => {
    return selectedValues.reduce(
        (finalStats, value) => _.mapValues(finalStats, (stat, statKey) => {
            let statValue = getStat(config, value, statKey)
            if (statKey === 'spirit' && playerClass === 'druid') {
                statValue *= 1.15
            }
            return stat + statValue
        }),
        { ...baseStats }
    )
}

export function selectCalculatedStats(state) {
    const selectedBuffsAndConsumes = [...state.character.raidBuffs, ...state.character.consumables]
    const modifier = _.sumBy(selectedBuffsAndConsumes, value => buffs[value]?.modifier || consumables[value]?.modifier || 0) + 1
    const baseStats = state.character.stats
    const playerClass = state.character.class

    let statsBeforeModifiers = reduceSelectedValuesToStats(state.character.raidBuffs, buffs, baseStats, playerClass)
    statsBeforeModifiers = reduceSelectedValuesToStats(state.character.consumables, consumables, statsBeforeModifiers, playerClass)
    return {
        intellect: Math.round(statsBeforeModifiers.intellect * modifier),
        spirit: Math.round(statsBeforeModifiers.spirit * modifier),
        mp5: Math.round(statsBeforeModifiers.mp5)
    }
}

export function selectTotalMana(state) {
    const calculatedStats = selectCalculatedStats(state)
    return CLASS_BASE_MANA[state.character.class] + (
        Math.min(20, calculatedStats.intellect) + // first 20 int contributes 20 mana
        15*(calculatedStats.intellect - Math.min(20, calculatedStats.intellect) ) // following int contribute 15 mana each
    )
}
