import {createAsyncThunk, createSlice} from '@reduxjs/toolkit'
import networkStatus from '../../models/networkStatus'
import superagent from 'superagent'
import {selectCalculatedStats, selectMeanSpellCost, selectTotalCpm, selectTotalMana} from './characterSelectors'

const initialState = {
    status: networkStatus.idle,
    simError: null,
    simResults: {
        // not sure what the structure should be yet
    },
    class: null,
    stats: {
        intellect: null,
        spirit: null,
        mp5: null,
    },
    raidBuffs: [],
    consumables: [],
    raidUtility: [],
    castsDistribution: {},
    procItems: [],
}

export const simulate = createAsyncThunk(
    'character/sim',
    async (payload, { rejectWithValue, getState }) => {
        const state = getState().character
        try {
            const config = {
                ...selectCalculatedStats(getState()),
                spellCost: selectMeanSpellCost(getState()),
                cpm: selectTotalCpm(getState()),
                consumables: state.consumables.filter(c => ['manaPot', 'rune'].includes(c)),
                raidUtility: state.raidUtility,
                procItems: state.procItems,
                totalMana: selectTotalMana(getState()),
                class: state.class,
            }
            const res = await superagent
                .get('/simulate')
                .query({
                    config: JSON.stringify(config)
                })
            return res.body
        } catch (err) {
            if (err.status) {
                // server error
                return rejectWithValue(err.response.text)
            } else {
                // network/timeout/other error
                return rejectWithValue(err)
            }
        }
    }
)

export const { reducer, actions } = createSlice({
    name: 'character',
    initialState,
    reducers: {
        setSelectedClass(state, { payload }) {
            state.class = payload
        },
        setBaseStats(state, { payload: baseStats }) {
            state.stats = {
                ...state.stats,
                ...baseStats
            }
        },
        setConfigToggles(state, { payload }) {
            const { key, values } = payload
            state[key] = values
        },
        setSpellCpm(state, { payload }) {
            const { id, ...rest } = payload
            const castsDistribution = {...state.castsDistribution}
            castsDistribution[id] = { ...castsDistribution[id], ...rest }
            state.castsDistribution = castsDistribution
        }
    },
    extraReducers: {
        [simulate.pending]: (state) => {
            state.status = networkStatus.loading
        },
        [simulate.fulfilled]: (state, { payload }) => {
            state.status = networkStatus.succeeded
            // load the sim results here
            state.simResults = payload
        },
        [simulate.rejected]: (state, { error }) => {
            state.status = networkStatus.idle
            state.error = error
        }
    }
})

export const { setSelectedClass, setBaseStats, setConfigToggles, setSpellCpm } = actions

export default reducer
