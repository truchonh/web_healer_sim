import {
    Typography,
    Toolbar,
    AppBar,
    Container,
    Button,
    Select,
    MenuItem,
    FormControl,
    InputLabel
} from '@mui/material'
import React from 'react'
import SimulationForm from './components/SimulationForm'
import {useDispatch, useSelector} from 'react-redux'
import {simulate, setSelectedClass, setConfigToggles} from './characterSlice'
import ResultChart from './components/ResultChart'
import {classes} from '../../models/Config'

export default function Sim() {
    const dispatch = useDispatch()
    const playerClass = useSelector(state => state.character.class)

    const onPlayerClassChanged = (e) => {
        dispatch(setSelectedClass(e.target.value))
        dispatch(setConfigToggles({
            key: 'raidUtility',
            values: []
        }))
    }

    return (
        <Container maxWidth="md" fixed>
            <AppBar position="fixed">
                <Toolbar>
                    <Typography variant="h6" sx={{
                        flexGrow: 1,
                        color: 'text.primary',
                    }}>
                        Mana regen optimization tool
                    </Typography>
                    <FormControl variant="filled" sx={{ m: 1, minWidth: 120 }}>
                        <InputLabel>Class</InputLabel>
                        <Select
                            value={playerClass}
                            onChange={onPlayerClassChanged}
                        >
                            {classes.map(wowClass => <MenuItem value={wowClass.value}>{wowClass.label}</MenuItem>)}
                        </Select>
                    </FormControl>
                </Toolbar>
            </AppBar>
            <SimulationForm />
            <Button variant="contained" onClick={() => dispatch(simulate())} disabled={!playerClass}>
                Simulate
            </Button>
            <ResultChart />
        </Container>
    )
}