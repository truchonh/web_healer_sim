import {Grid} from '@mui/material'
import React, {useMemo} from 'react'
import {Line} from 'react-chartjs-2'
import {useSelector} from 'react-redux'
import _ from 'lodash'

const MAX_FIGHT_LENGTH_SECONDS = 1200;

export default function() {
    const simResult = useSelector(state => state.character.simResults)

    const chartData = useMemo(() => {
        if (_.isEmpty(simResult)) {
            return []
        }

        const selectedItem = simResult.items.filter(item => simResult.config.procItems.includes(item.item))
        const offsets = [
            { label: 'mean', value: 0 },
        ]
        if (!_.isEmpty(selectedItem)) {
            offsets.push({ label: 'sig1', color: 'rgba(121,134,203,0.5)', value: _.sumBy(selectedItem, item => item.sig1 - item.avg) })
            offsets.push({ label: 'sig2', color: 'rgba(121,134,203,0.3)', value: _.sumBy(selectedItem, item => item.sig2 - item.avg) })
        }

        return offsets.reduce((datasets, mp5Offset) => {
            const simulateManaSpent = (manaSpentPerSecond, offset = 0) => {
                const estimatedDurationSeconds = Math.min(simResult.config.totalMana / (manaSpentPerSecond + offset), MAX_FIGHT_LENGTH_SECONDS)
                const fillHackEstimatedDuration = offset > 0
                    ? Math.min(simResult.config.totalMana / (manaSpentPerSecond - offset), MAX_FIGHT_LENGTH_SECONDS)
                    : estimatedDurationSeconds
                const tickCount = Math.round(fillHackEstimatedDuration / (60 / simResult.config.cpm))
                const tickLengthMs = fillHackEstimatedDuration * 1000 / tickCount

                const data = []
                let simulatedMana = simResult.config.totalMana

                for (let i = 0; i < tickCount; i++) {
                    simulatedMana = simResult.config.totalMana - (
                        (i * tickLengthMs / 1000) * (manaSpentPerSecond + offset)
                    )
                    if (simulatedMana < 0) {
                        simulatedMana = 0
                    }
                    data.push({
                        x: Math.floor(i * tickLengthMs),
                        y: Math.round(simulatedMana)
                    })
                }

                return data
            }

            const dataset = {
                id: datasets.length + 1,
                type: 'line',
                label: mp5Offset.label,
                elements: {
                    line: {
                        borderColor: mp5Offset.color
                    }
                },
            }

            if (mp5Offset.label === 'mean') {
                const manaSpentPerSecond = simResult.config.totalMana / simResult.estimatedDurationSeconds
                datasets.push({
                    ...dataset,
                    data: simulateManaSpent(manaSpentPerSecond),
                })
            } else {

                const manaSpentPerSecond = simResult.config.totalMana / simResult.estimatedDurationSeconds
                const manaSpentOffset = mp5Offset.value / 5
                datasets.push({
                    ...dataset,
                    data: simulateManaSpent(manaSpentPerSecond, -manaSpentOffset),
                    elements: {
                        line: {
                            borderColor: mp5Offset.color,
                            borderWidth: 1
                        }
                    },
                    fill: {
                        target: '+1',
                        above: mp5Offset.color
                    }
                })
                datasets.push({
                    ...dataset,
                    data: simulateManaSpent(manaSpentPerSecond, manaSpentOffset),
                    elements: {
                        line: {
                            borderColor: mp5Offset.color,
                            borderWidth: 1
                        }
                    },
                })

            }

            return datasets
        }, [])
    }, [simResult])

    return (
        <Grid container spacing={0}>
            <Line
                datasetIdKey="id"
                data={{
                    labels: ['1', '2', '3'],
                    datasets: chartData
                }}
                options={{
                    plugins: {
                        legend: {
                            display: false
                        }
                    },
                    elements: {
                        point: {
                            radius: 3,
                            borderColor: 'transparent',
                            backgroundColor: 'transparent'
                        },
                        line: {
                            borderColor: 'white',
                            borderWidth: 3
                        }
                    },
                    scales: {
                        x: {
                            type: 'time',
                            time: {
                                unit: 'millisecond',
                                displayFormats: {
                                    millisecond: 'm:ss'
                                },
                                tooltipFormat: 'm:ss',
                                stepSize: 20
                            },
                            parsing: false,
                            grid: {
                                color: 'rgba(255, 255, 255, 0.1)'
                            },
                            ticks: {
                                color: 'rgba(255, 255, 255, 0.7)'
                            }
                        },
                        y: {
                            grid: {
                                color: 'rgba(255, 255, 255, 0.1)'
                            },
                            ticks: {
                                color: 'rgba(255, 255, 255, 0.7)'
                            }
                        }
                    }
                }}
            />
        </Grid>
    )
}
