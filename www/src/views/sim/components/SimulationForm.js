import {Box, FormControl, FormLabel, Grid} from '@mui/material'
import {buffs, consumables, items, raidUtility} from '../../../models/Config'
import {setBaseStats} from '../characterSlice'
import {useDispatch, useSelector} from 'react-redux'
import ConfigGroup from './ConfigGroup'
import CastConfigTable from './CastsConfigTable'
import WarcraftLogsInput from './WarcraftLogsInput'
import NumericField from '../../../fields/NumericField'
import _ from 'lodash'

export default function SimulationForm() {
    const dispatch = useDispatch()
    const playerClass = useSelector(state => state.character.class)
    const baseStats = useSelector(state => state.character.stats)

    const onStatsChanged = (event) => {
        const { name, value } = event.target
        dispatch(setBaseStats({
            ...baseStats,
            [name]: value
        }))
    }

    const filterConfigKeys = (config) => {
        return _.pickBy(config, (configEntry) => {
            if (configEntry.classes) {
                return configEntry.classes.includes(playerClass)
            }
            return true
        })
    }

    return (
        <>
            <Grid container spacing={2}>
                <FormControl sx={{ m: 3 }} component="fieldset" variant="standard">
                    <FormLabel component="legend">Base stats</FormLabel>
                    <Box sx={{
                        '& .MuiTextField-root': { m: 1, width: '18ch' },
                    }}>
                        <NumericField
                            name="intellect"
                            label="Intellect"
                            value={baseStats.intellect}
                            onChange={onStatsChanged}
                            size="small"
                        />
                        <NumericField
                            name="spirit"
                            label="Spirit"
                            value={baseStats.spirit}
                            onChange={onStatsChanged}
                            size="small"
                        />
                        <NumericField
                            name="mp5"
                            label="Mana per 5 sec."
                            value={baseStats.mp5}
                            onChange={onStatsChanged}
                            size="small"
                        />
                    </Box>
                </FormControl>
            </Grid>
            <Grid container spacing={2}>
                <ConfigGroup title="Raid buffs" stateKey="raidBuffs" config={filterConfigKeys(buffs)} />
            </Grid>
            <Grid container spacing={2}>
                <ConfigGroup title="Consumables" stateKey="consumables" config={filterConfigKeys(consumables)} />
            </Grid>
            <Grid container spacing={2}>
                <ConfigGroup title="Raid Utility" stateKey="raidUtility" config={filterConfigKeys(raidUtility)} />
            </Grid>
            <Grid container spacing={2} hidden={!playerClass}>
                <CastConfigTable />
                <WarcraftLogsInput />
            </Grid>
            <Grid container spacing={2}>
                <ConfigGroup title="Items with proc effect" stateKey="procItems" config={filterConfigKeys(items)} />
            </Grid>
        </>
    )
}
