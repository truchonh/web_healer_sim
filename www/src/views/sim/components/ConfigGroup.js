import {Checkbox, FormControl, FormControlLabel, FormGroup, FormLabel} from '@mui/material'
import _ from 'lodash'
import React, {useEffect, useState} from 'react'
import {useDispatch, useSelector} from 'react-redux'
import {setConfigToggles} from '../characterSlice'

export default function ConfigGroup(props) {
    const { config, title, stateKey } = props
    const dispatch = useDispatch()
    const [toggles, setToggles] = useState({
        ..._.mapValues(config, () => false)
    })
    const stateToggles = useSelector(state => state.character[stateKey])

    useEffect(() => {
        setToggles({
            ..._.mapValues(config, (val, key) => stateToggles.includes(key))
        })
    }, [stateToggles])

    const handleChange = (event) => {
        const { name, checked } = event.target
        setToggles(prevState => {
            const newState = {
                ...prevState,
                [name]: checked
            }
            const values = _.chain(newState).map((val, key) => val && key).filter(Boolean).value()
            dispatch(setConfigToggles({ key: stateKey, values }))
            return newState
        })
    }

    return (
        <FormControl sx={{ m: 3 }} component="fieldset" variant="standard">
            <FormLabel component="legend">{title}</FormLabel>
            <FormGroup row={true}>
                {_.map(config, (item, key) => (
                    <FormControlLabel key={key} label={item.name} control={
                        <Checkbox name={key} checked={toggles[key]} onChange={handleChange} />
                    } />
                ))}
            </FormGroup>
        </FormControl>
    )
}