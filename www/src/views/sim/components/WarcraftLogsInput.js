import {Button, FormControl, Grid, InputLabel, MenuItem, OutlinedInput, Select} from '@mui/material'
import React, {useEffect, useState} from 'react'
import superagent from 'superagent'
import _ from 'lodash'
import {spells} from '../../../models/Config'
import {setSpellCpm} from '../characterSlice'
import {useDispatch, useSelector} from 'react-redux'

const HEALER_SPECS = ['Restoration', 'Holy', 'Discipline']

export default function() {
    const dispatch = useDispatch()
    const playerClass = useSelector(state => state.character.class)
    const [reportCode, setReportCode] = useState('')
    const [wlToken, setWlToken] = useState('')
    const [fights, setFights] = useState([])
    const [players, setPlayers] = useState([])
    const [selectedFightId, setSelectedFightId] = useState(null)
    const [selectedPlayerId, setSelectedPlayerId] = useState(null)

    const onLogUrlChanged = (e) => {
        const value = e.target.value
        const logCodeMatches = /(?!https:\/\/classic\.warcraftlogs\.com\/reports\/)([a-z0-9]+)((?=#)|$)/i.exec(value)
        if (value && logCodeMatches.length) {
            setReportCode(logCodeMatches[0])
        }
    }

    useEffect(() => {
        async function _getReport() {
            const res = await superagent
                .get(`https://classic.warcraftlogs.com:443/v1/report/fights/${reportCode}`)
                .query({ api_key: wlToken })
            setFights(
                res.body?.fights.filter(fight => fight.boss > 0 && fight.kill) || []
            )
            setPlayers(
                _.chain(res.body?.friendlies || [])
                    .filter(player => HEALER_SPECS.some(spec => player.icon.includes(spec)))
                    .sortBy(player => player.name)
                    .value()
            )
        }
        if (reportCode && wlToken) {
            _getReport()
        } else {
            setFights([])
        }
    }, [reportCode, wlToken])

    const fillCastTable = async () => {
        const fight = fights.find(fight => fight.id === selectedFightId)
        const res = await superagent
            .get(`https://classic.warcraftlogs.com:443/v1/report/tables/casts/${reportCode}`)
            .query({
                start: fight.start_time,
                end: fight.end_time,
                sourceid: selectedPlayerId,
                api_key: wlToken
            })
        const fightLengthMinutes = (fight.end_time - fight.start_time) / (1000 * 60)
        for (let spell of spells[playerClass]) {
            const logEntry = res.body.entries.find(entry => parseInt(spell.id) === entry.guid)
            dispatch(setSpellCpm({
                id: spell.id,
                cpm: logEntry ? _.round(logEntry.total / fightLengthMinutes, 2) : 0,
            }))
        }
    }

    return (
        <Grid sx={{ m: 2 }} container spacing={2}>
            <Grid item xs={8}>
                <FormControl fullWidth>
                    <InputLabel htmlFor="outlined-adornment-amount">Warcraft Logs URL</InputLabel>
                    <OutlinedInput
                        id="outlined-adornment-amount"
                        onChange={onLogUrlChanged}
                    />
                </FormControl>
            </Grid>
            <Grid item xs={4}>
                <FormControl fullWidth>
                    <InputLabel htmlFor="outlined-adornment-amount">API token</InputLabel>
                    <OutlinedInput
                        id="outlined-adornment-amount"
                        onChange={(e) => setWlToken(e.target.value)}
                    />
                </FormControl>
            </Grid>
            <Grid item xs={4} hidden={_.isEmpty(fights) || _.isEmpty(players)}>
                <FormControl fullWidth>
                    <InputLabel>Fights</InputLabel>
                    <Select
                        value={selectedFightId}
                        onChange={(e) => setSelectedFightId(e.target.value)}
                    >
                        {fights.map(fight => (
                            <MenuItem value={fight.id}>{fight.name}</MenuItem>
                        ))}
                    </Select>
                </FormControl>
            </Grid>
            <Grid item xs={4} hidden={_.isEmpty(fights) || _.isEmpty(players)}>
                <FormControl fullWidth>
                    <InputLabel>Player</InputLabel>
                    <Select
                        value={selectedPlayerId}
                        onChange={(e) => setSelectedPlayerId(e.target.value)}
                    >
                        {players.map(player => (
                            <MenuItem value={player.id}>{player.name}</MenuItem>
                        ))}
                    </Select>
                </FormControl>
            </Grid>
            <Grid item xs={4} hidden={_.isEmpty(fights) || _.isEmpty(players)}>
                <Button
                    variant="outlined"
                    disabled={!selectedFightId || !selectedPlayerId}
                    onClick={fillCastTable}
                >
                    Fill cast table
                </Button>
            </Grid>
        </Grid>
    )
}