import {FormControl, FormLabel} from '@mui/material'
import {DataGrid} from '@mui/x-data-grid'
import React, {useMemo, useRef} from 'react'
import {useDispatch, useSelector} from 'react-redux'
import {setSpellCpm} from '../characterSlice'
import {spells} from '../../../models/Config'
import _ from 'lodash'
import {selectMeanSpellCost, selectTotalCpm} from '../characterSelectors'

const columns = [
    { field: 'name', headerName: 'Ability', width: 150 },
    { field: 'cost', headerName: 'Cost', type: 'number', width: 90 },
    {
        field: 'cpm',
        headerName: 'CPM',
        type: 'number',
        width: 90,
        editable: true,
    },
    {
        field: 'snowball',
        headerName: 'Snowball macroed',
        type: 'boolean',
        width: 180,
        editable: true,
    },
];

export default function CastConfigTable() {
    const dispatch = useDispatch()
    const castsDistribution = useSelector(state => state.character.castsDistribution)
    const playerClass = useSelector(state => state.character.class)
    const totalCpm = useSelector(selectTotalCpm)
    const spellCost = useSelector(selectMeanSpellCost)

    const onTableEdit = (params) => {
        dispatch(setSpellCpm({
            id: params.id,
            [params.field]: params.value,
        }))
    }

    const rows = useMemo(() => {
        if (!playerClass) {
            return []
        }
        let _rows = [...spells[playerClass]]
        _.each(castsDistribution, (spellConfig, id) => {
            let rowIndex = _rows.findIndex(row => row.id === id)
            _rows[rowIndex] = {
                ..._rows[rowIndex],
                ...spellConfig
            }
        })
        return _rows
    }, [castsDistribution, playerClass, spells])

    return (
        <FormControl sx={{ m: 3, width: '100%' }} component="fieldset" variant="standard">
            <FormLabel component="legend">Spell cast distribution</FormLabel>
            <div>Total cpm: {totalCpm}</div>
            <div>Avg spell cost: {spellCost}</div>
            <div style={{ height: 400, width: '100%' }}>
                <DataGrid
                    density="compact"
                    sx={{ m: 1 }}
                    rows={rows}
                    columns={columns}
                    pageSize={8}
                    disableSelectionOnClick
                    rowsPerPageOptions={[8]}
                    onCellEditCommit={onTableEdit}
                />
            </div>
            {/* TODO: add a JoW uptime field for snowball mana regen calculation */}
        </FormControl>
    )
}
