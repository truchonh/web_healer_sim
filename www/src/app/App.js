import React from 'react'
import Sim from '../views/sim/Sim'
import {BrowserRouter as Router, Routes, Route} from 'react-router-dom'
import {Box, createTheme, CssBaseline} from '@mui/material'
import {ThemeProvider} from '@emotion/react'

const darkTheme = createTheme({
    palette: {
        mode: 'dark',
        primary: {
            main: '#7986cb'
        }
    },
})

export default function App() {
    return (
        <Router>
            <CssBaseline />
            <ThemeProvider theme={darkTheme}>
                <Box sx={{
                    backgroundColor: '#282c34',
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                    minHeight: '100vh',
                    color: darkTheme.palette.text.primary
                }} pt={10}>
                    <Routes>
                        <Route path="/" element={<Sim/>} />
                    </Routes>
                </Box>
            </ThemeProvider>
        </Router>
    )
}
