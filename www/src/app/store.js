import { configureStore } from '@reduxjs/toolkit'
import characterSlice from '../views/sim/characterSlice'
import {loadState, saveState} from './localStorage'
import {debounce} from 'lodash'
import _ from 'lodash'

const preloadedState = loadState()

const store = configureStore({
    reducer: {
        character: characterSlice
    },
    preloadedState
})

// persist the 'character' part of the state in localstorage
// @ref https://medium.com/@jrcreencia/persisting-redux-state-to-local-storage-f81eb0b90e7e
store.subscribe(debounce(() => {
    saveState({
        character: _.omit(store.getState().character, ['simResults', 'simError'])
    })
}, 1000))

export default store
