const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const https = require('https')
const fs = require('fs');
const simCtrl = require('./api/simCtrl');

require('dotenv').config();

class Server {
    static async start() {
        const app = express();

        app.use(bodyParser.urlencoded({ extended: true, limit: '5mb' }));
        app.use(bodyParser.json({ limit: '5mb' }));

        app.use(express.static(path.join(__dirname, 'www', 'build')));

        simCtrl.init(app);

        app.get('/*', (req, res) => {
            res.sendFile(path.join(__dirname, 'www', 'build', 'index.html'));
        });

        try {
            const credentials = {
                key: fs.readFileSync(process.env.HTTPS_PRIVK_PATH, 'utf8'),
                cert: fs.readFileSync(process.env.HTTPS_CERT_PATH, 'utf8')
            };
            let httpsServer = https.createServer(credentials, app);
            await httpsServer.listen(process.env.HTTPS_PORT);

            // http to https redirect
            const http = express();
            http.get('*', function(req, res) {
                res.redirect('https://' + req.headers.host + req.url);
            })
            http.listen(process.env.PORT || 80);
            console.log(`https server started on port ${process.env.HTTPS_PORT}`);
        } catch (err) {
            console.error('Could not load the https credentials.');

            await app.listen(process.env.PORT || 80);
        }
    }
}

Server.start()
    .catch(err => {
        console.error(err);
        process.exit(1);
    });
