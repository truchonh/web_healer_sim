FROM ubuntu
EXPOSE 443/tcp
EXPOSE 80/tcp

# prevent interactive prompts (issue with tzdata)
ENV DEBIAN_FRONTEND=noninteractive

# update basic packages
RUN apt-get update
RUN apt-get install -y gconf-service libxext6 libxfixes3 libxi6 libxrandr2 libxrender1 libcairo2 libcups2 \
libdbus-1-3 libexpat1 libfontconfig1 libgcc1 libgconf-2-4 libgdk-pixbuf2.0-0 libglib2.0-0 libgtk-3-0 \
libnspr4 libpango-1.0-0 libpangocairo-1.0-0 libstdc++6 libx11-6 libx11-xcb1 libxcb1 libxcomposite1 \
libxcursor1 libxdamage1 libxss1 libxtst6 libappindicator1 libnss3 libasound2 libatk1.0-0 libc6 ca-certificates \
fonts-liberation lsb-release xdg-utils wget

# install node
RUN apt-get install -y curl
RUN curl -sL https://deb.nodesource.com/setup_14.x | bash -
RUN apt-get install -y nodejs

# install yarn
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
RUN apt update && apt-get install yarn -y
RUN echo "alias node=nodejs" >> ~/.bashrc

# front-end build
WORKDIR /home/ubuntu/web_healer_sim
COPY ./www ./www
WORKDIR /home/ubuntu/web_healer_sim/www
RUN yarn
RUN yarn build

# install api dependencies
WORKDIR /home/ubuntu/web_healer_sim
COPY ./package.json ./package.json
COPY ./yarn.lock ./yarn.lock
WORKDIR /home/ubuntu/web_healer_sim
RUN yarn

# copy api source code
WORKDIR /home/ubuntu/web_healer_sim
COPY . .

# start !
WORKDIR /home/ubuntu/web_healer_sim
CMD ["node", "/home/ubuntu/web_healer_sim/server.js"]